﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatientMonitorDL
{
    class databaseConnection
    {
       //Attributes of the Database Connection Class
        private string connectionString;

        System.Data.SqlClient.SqlConnection connectionDatabase;

        private System.Data.SqlClient.SqlDataAdapter dataAdapter;

        //Constructors for the Database Class
        public databaseConnection(string connectionString)
        {
            this.connectionString = connectionString;
        }

        //Method for opening connection
        public void openConnection()
        {
            connectionDatabase = new System.Data.SqlClient.SqlConnection(connectionString);
            connectionDatabase.Open();
        }

        //Method for closing the connection
        public void closeConnection()
        {
            connectionDatabase.Close();
        }

        //Setting up the dataset to the database 
        public System.Data.DataSet getDataSet(string sqlStatement)
        {
            System.Data.DataSet dataSet;
            // create the object dataAdapter 
            dataAdapter = new System.Data.SqlClient.SqlDataAdapter(sqlStatement, connectionDatabase);

            // create the dataset
            dataSet = new System.Data.DataSet();
            dataAdapter.Fill(dataSet);

             //return the dataSet
             return dataSet;
}

    }
}
