﻿namespace PatientMonitorDL
{
    partial class ClockIn
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnClockIn2 = new System.Windows.Forms.Button();
            this.lstConsultants = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // btnClockIn2
            // 
            this.btnClockIn2.Location = new System.Drawing.Point(12, 201);
            this.btnClockIn2.Name = "btnClockIn2";
            this.btnClockIn2.Size = new System.Drawing.Size(470, 68);
            this.btnClockIn2.TabIndex = 0;
            this.btnClockIn2.Text = "Clock In";
            this.btnClockIn2.UseVisualStyleBackColor = true;
            // 
            // lstConsultants
            // 
            this.lstConsultants.FormattingEnabled = true;
            this.lstConsultants.Items.AddRange(new object[] {
            "Dr Sam Jones",
            "Dr Aled Smith",
            "Dr William Lewis",
            "Dr Fiona Class",
            "Dr Wendy Haslam",
            "Dr Racheal Dawe",
            "Dr Joe Bloggs",
            "Prof Simon Goods",
            "Prof David Jordan",
            "Nurse Ethan Chin",
            "Nurse Alexa Skirsnski "});
            this.lstConsultants.Location = new System.Drawing.Point(12, 12);
            this.lstConsultants.Name = "lstConsultants";
            this.lstConsultants.Size = new System.Drawing.Size(470, 173);
            this.lstConsultants.TabIndex = 1;
            // 
            // ClockIn
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(494, 284);
            this.Controls.Add(this.lstConsultants);
            this.Controls.Add(this.btnClockIn2);
            this.Name = "ClockIn";
            this.Text = "ClockIn";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnClockIn2;
        private System.Windows.Forms.ListBox lstConsultants;
    }
}