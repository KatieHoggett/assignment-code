﻿namespace PatientMonitorDL
{
    partial class PatientMonitoringMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabBay1 = new System.Windows.Forms.TabPage();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.B1G4Max = new System.Windows.Forms.NumericUpDown();
            this.B1G4Min = new System.Windows.Forms.NumericUpDown();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.B1G3Max = new System.Windows.Forms.NumericUpDown();
            this.B1G3Min = new System.Windows.Forms.NumericUpDown();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.B1G2Max = new System.Windows.Forms.NumericUpDown();
            this.B1G2Min = new System.Windows.Forms.NumericUpDown();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.B1G1Min = new System.Windows.Forms.NumericUpDown();
            this.B1G1Max = new System.Windows.Forms.NumericUpDown();
            this.tabBay2 = new System.Windows.Forms.TabPage();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.B2G4Max = new System.Windows.Forms.NumericUpDown();
            this.B2G4Min = new System.Windows.Forms.NumericUpDown();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.B2G3Min = new System.Windows.Forms.NumericUpDown();
            this.B2GMin = new System.Windows.Forms.NumericUpDown();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.B2G2Max = new System.Windows.Forms.NumericUpDown();
            this.B2G2Min = new System.Windows.Forms.NumericUpDown();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.B2G1Max = new System.Windows.Forms.NumericUpDown();
            this.B2G1Min = new System.Windows.Forms.NumericUpDown();
            this.tabBay3 = new System.Windows.Forms.TabPage();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.B3G4Max = new System.Windows.Forms.NumericUpDown();
            this.B3G4Min = new System.Windows.Forms.NumericUpDown();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.B3G3Max = new System.Windows.Forms.NumericUpDown();
            this.B3G3Min = new System.Windows.Forms.NumericUpDown();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.B3G2Max = new System.Windows.Forms.NumericUpDown();
            this.B3G2Min = new System.Windows.Forms.NumericUpDown();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.B3G1Max = new System.Windows.Forms.NumericUpDown();
            this.B3G1Min = new System.Windows.Forms.NumericUpDown();
            this.tabBay4 = new System.Windows.Forms.TabPage();
            this.tabBay5 = new System.Windows.Forms.TabPage();
            this.tabBay6 = new System.Windows.Forms.TabPage();
            this.tabBay7 = new System.Windows.Forms.TabPage();
            this.tabBay8 = new System.Windows.Forms.TabPage();
            this.btnClockIn = new System.Windows.Forms.Button();
            this.btnClockOut = new System.Windows.Forms.Button();
            this.lstOnCall = new System.Windows.Forms.ListBox();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this.groupBox15 = new System.Windows.Forms.GroupBox();
            this.groupBox16 = new System.Windows.Forms.GroupBox();
            this.B4G1Min = new System.Windows.Forms.NumericUpDown();
            this.B4G1Max = new System.Windows.Forms.NumericUpDown();
            this.B4G2Min = new System.Windows.Forms.NumericUpDown();
            this.B4G2Max = new System.Windows.Forms.NumericUpDown();
            this.B4G3Min = new System.Windows.Forms.NumericUpDown();
            this.B4G3Max = new System.Windows.Forms.NumericUpDown();
            this.B4G4Min = new System.Windows.Forms.NumericUpDown();
            this.B4G4Max = new System.Windows.Forms.NumericUpDown();
            this.groupBox17 = new System.Windows.Forms.GroupBox();
            this.groupBox18 = new System.Windows.Forms.GroupBox();
            this.groupBox19 = new System.Windows.Forms.GroupBox();
            this.groupBox20 = new System.Windows.Forms.GroupBox();
            this.B5G1Min = new System.Windows.Forms.NumericUpDown();
            this.B5G1Max = new System.Windows.Forms.NumericUpDown();
            this.B5G2Min = new System.Windows.Forms.NumericUpDown();
            this.B5G2Max = new System.Windows.Forms.NumericUpDown();
            this.B5G3Max = new System.Windows.Forms.NumericUpDown();
            this.B5G3Min = new System.Windows.Forms.NumericUpDown();
            this.B5G4Max = new System.Windows.Forms.NumericUpDown();
            this.B5G4Min = new System.Windows.Forms.NumericUpDown();
            this.groupBox21 = new System.Windows.Forms.GroupBox();
            this.groupBox22 = new System.Windows.Forms.GroupBox();
            this.groupBox23 = new System.Windows.Forms.GroupBox();
            this.groupBox24 = new System.Windows.Forms.GroupBox();
            this.B6G1Min = new System.Windows.Forms.NumericUpDown();
            this.B6G1Max = new System.Windows.Forms.NumericUpDown();
            this.B6G2Max = new System.Windows.Forms.NumericUpDown();
            this.B6G2Min = new System.Windows.Forms.NumericUpDown();
            this.B6G4Max = new System.Windows.Forms.NumericUpDown();
            this.B6G4Min = new System.Windows.Forms.NumericUpDown();
            this.B6G3Max = new System.Windows.Forms.NumericUpDown();
            this.B6G3Min = new System.Windows.Forms.NumericUpDown();
            this.groupBox25 = new System.Windows.Forms.GroupBox();
            this.groupBox26 = new System.Windows.Forms.GroupBox();
            this.groupBox27 = new System.Windows.Forms.GroupBox();
            this.groupBox28 = new System.Windows.Forms.GroupBox();
            this.B7G1Min = new System.Windows.Forms.NumericUpDown();
            this.B7G1Max = new System.Windows.Forms.NumericUpDown();
            this.B7G3Max = new System.Windows.Forms.NumericUpDown();
            this.B7G3Min = new System.Windows.Forms.NumericUpDown();
            this.B7G4Max = new System.Windows.Forms.NumericUpDown();
            this.B7G4Min = new System.Windows.Forms.NumericUpDown();
            this.B7G2Max = new System.Windows.Forms.NumericUpDown();
            this.B7G2Min = new System.Windows.Forms.NumericUpDown();
            this.groupBox29 = new System.Windows.Forms.GroupBox();
            this.groupBox30 = new System.Windows.Forms.GroupBox();
            this.groupBox31 = new System.Windows.Forms.GroupBox();
            this.groupBox32 = new System.Windows.Forms.GroupBox();
            this.B8G1Min = new System.Windows.Forms.NumericUpDown();
            this.B8G1Max = new System.Windows.Forms.NumericUpDown();
            this.B8G2Max = new System.Windows.Forms.NumericUpDown();
            this.B8G2Min = new System.Windows.Forms.NumericUpDown();
            this.B8G3Max = new System.Windows.Forms.NumericUpDown();
            this.B8G3Min = new System.Windows.Forms.NumericUpDown();
            this.B8G4Max = new System.Windows.Forms.NumericUpDown();
            this.B8G4Min = new System.Windows.Forms.NumericUpDown();
            this.tabControl1.SuspendLayout();
            this.tabBay1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.B1G4Max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.B1G4Min)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.B1G3Max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.B1G3Min)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.B1G2Max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.B1G2Min)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.B1G1Min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.B1G1Max)).BeginInit();
            this.tabBay2.SuspendLayout();
            this.groupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.B2G4Max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.B2G4Min)).BeginInit();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.B2G3Min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.B2GMin)).BeginInit();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.B2G2Max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.B2G2Min)).BeginInit();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.B2G1Max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.B2G1Min)).BeginInit();
            this.tabBay3.SuspendLayout();
            this.groupBox12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.B3G4Max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.B3G4Min)).BeginInit();
            this.groupBox11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.B3G3Max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.B3G3Min)).BeginInit();
            this.groupBox10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.B3G2Max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.B3G2Min)).BeginInit();
            this.groupBox9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.B3G1Max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.B3G1Min)).BeginInit();
            this.tabBay4.SuspendLayout();
            this.tabBay5.SuspendLayout();
            this.tabBay6.SuspendLayout();
            this.tabBay7.SuspendLayout();
            this.tabBay8.SuspendLayout();
            this.groupBox13.SuspendLayout();
            this.groupBox14.SuspendLayout();
            this.groupBox15.SuspendLayout();
            this.groupBox16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.B4G1Min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.B4G1Max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.B4G2Min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.B4G2Max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.B4G3Min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.B4G3Max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.B4G4Min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.B4G4Max)).BeginInit();
            this.groupBox17.SuspendLayout();
            this.groupBox18.SuspendLayout();
            this.groupBox19.SuspendLayout();
            this.groupBox20.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.B5G1Min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.B5G1Max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.B5G2Min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.B5G2Max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.B5G3Max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.B5G3Min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.B5G4Max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.B5G4Min)).BeginInit();
            this.groupBox21.SuspendLayout();
            this.groupBox22.SuspendLayout();
            this.groupBox23.SuspendLayout();
            this.groupBox24.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.B6G1Min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.B6G1Max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.B6G2Max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.B6G2Min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.B6G4Max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.B6G4Min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.B6G3Max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.B6G3Min)).BeginInit();
            this.groupBox25.SuspendLayout();
            this.groupBox26.SuspendLayout();
            this.groupBox27.SuspendLayout();
            this.groupBox28.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.B7G1Min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.B7G1Max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.B7G3Max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.B7G3Min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.B7G4Max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.B7G4Min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.B7G2Max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.B7G2Min)).BeginInit();
            this.groupBox29.SuspendLayout();
            this.groupBox30.SuspendLayout();
            this.groupBox31.SuspendLayout();
            this.groupBox32.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.B8G1Min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.B8G1Max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.B8G2Max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.B8G2Min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.B8G3Max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.B8G3Min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.B8G4Max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.B8G4Min)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabBay1);
            this.tabControl1.Controls.Add(this.tabBay2);
            this.tabControl1.Controls.Add(this.tabBay3);
            this.tabControl1.Controls.Add(this.tabBay4);
            this.tabControl1.Controls.Add(this.tabBay5);
            this.tabControl1.Controls.Add(this.tabBay6);
            this.tabControl1.Controls.Add(this.tabBay7);
            this.tabControl1.Controls.Add(this.tabBay8);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(675, 511);
            this.tabControl1.TabIndex = 0;
            // 
            // tabBay1
            // 
            this.tabBay1.BackColor = System.Drawing.Color.Transparent;
            this.tabBay1.Controls.Add(this.groupBox4);
            this.tabBay1.Controls.Add(this.groupBox3);
            this.tabBay1.Controls.Add(this.groupBox2);
            this.tabBay1.Controls.Add(this.groupBox1);
            this.tabBay1.Location = new System.Drawing.Point(4, 22);
            this.tabBay1.Name = "tabBay1";
            this.tabBay1.Padding = new System.Windows.Forms.Padding(3);
            this.tabBay1.Size = new System.Drawing.Size(667, 485);
            this.tabBay1.TabIndex = 0;
            this.tabBay1.Text = "Bay 1";
            this.tabBay1.Click += new System.EventHandler(this.tabBay1_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.B1G4Max);
            this.groupBox4.Controls.Add(this.B1G4Min);
            this.groupBox4.Location = new System.Drawing.Point(354, 248);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(292, 208);
            this.groupBox4.TabIndex = 5;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "groupBox4";
            // 
            // B1G4Max
            // 
            this.B1G4Max.Location = new System.Drawing.Point(212, 104);
            this.B1G4Max.Maximum = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.B1G4Max.Name = "B1G4Max";
            this.B1G4Max.Size = new System.Drawing.Size(42, 20);
            this.B1G4Max.TabIndex = 1;
            // 
            // B1G4Min
            // 
            this.B1G4Min.Location = new System.Drawing.Point(51, 104);
            this.B1G4Min.Maximum = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.B1G4Min.Name = "B1G4Min";
            this.B1G4Min.Size = new System.Drawing.Size(36, 20);
            this.B1G4Min.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.B1G3Max);
            this.groupBox3.Controls.Add(this.B1G3Min);
            this.groupBox3.Location = new System.Drawing.Point(16, 248);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(312, 208);
            this.groupBox3.TabIndex = 4;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "groupBox3";
            // 
            // B1G3Max
            // 
            this.B1G3Max.Location = new System.Drawing.Point(231, 105);
            this.B1G3Max.Maximum = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.B1G3Max.Name = "B1G3Max";
            this.B1G3Max.Size = new System.Drawing.Size(38, 20);
            this.B1G3Max.TabIndex = 1;
            // 
            // B1G3Min
            // 
            this.B1G3Min.Location = new System.Drawing.Point(44, 105);
            this.B1G3Min.Maximum = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.B1G3Min.Name = "B1G3Min";
            this.B1G3Min.Size = new System.Drawing.Size(41, 20);
            this.B1G3Min.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.B1G2Max);
            this.groupBox2.Controls.Add(this.B1G2Min);
            this.groupBox2.Location = new System.Drawing.Point(354, 17);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(292, 206);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "groupBox2";
            // 
            // B1G2Max
            // 
            this.B1G2Max.Location = new System.Drawing.Point(218, 97);
            this.B1G2Max.Maximum = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.B1G2Max.Name = "B1G2Max";
            this.B1G2Max.Size = new System.Drawing.Size(36, 20);
            this.B1G2Max.TabIndex = 1;
            // 
            // B1G2Min
            // 
            this.B1G2Min.Location = new System.Drawing.Point(51, 98);
            this.B1G2Min.Maximum = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.B1G2Min.Name = "B1G2Min";
            this.B1G2Min.Size = new System.Drawing.Size(36, 20);
            this.B1G2Min.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.B1G1Min);
            this.groupBox1.Controls.Add(this.B1G1Max);
            this.groupBox1.Location = new System.Drawing.Point(16, 17);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(312, 206);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "groupBox1";
            // 
            // B1G1Min
            // 
            this.B1G1Min.Location = new System.Drawing.Point(31, 98);
            this.B1G1Min.Maximum = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.B1G1Min.Name = "B1G1Min";
            this.B1G1Min.Size = new System.Drawing.Size(33, 20);
            this.B1G1Min.TabIndex = 2;
            // 
            // B1G1Max
            // 
            this.B1G1Max.Location = new System.Drawing.Point(246, 98);
            this.B1G1Max.Maximum = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.B1G1Max.Name = "B1G1Max";
            this.B1G1Max.Size = new System.Drawing.Size(33, 20);
            this.B1G1Max.TabIndex = 1;
            this.B1G1Max.Value = new decimal(new int[] {
            90,
            0,
            0,
            0});
            // 
            // tabBay2
            // 
            this.tabBay2.Controls.Add(this.groupBox8);
            this.tabBay2.Controls.Add(this.groupBox7);
            this.tabBay2.Controls.Add(this.groupBox6);
            this.tabBay2.Controls.Add(this.groupBox5);
            this.tabBay2.Location = new System.Drawing.Point(4, 22);
            this.tabBay2.Name = "tabBay2";
            this.tabBay2.Padding = new System.Windows.Forms.Padding(3);
            this.tabBay2.Size = new System.Drawing.Size(667, 485);
            this.tabBay2.TabIndex = 1;
            this.tabBay2.Text = "Bay 2";
            this.tabBay2.UseVisualStyleBackColor = true;
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.B2G4Max);
            this.groupBox8.Controls.Add(this.B2G4Min);
            this.groupBox8.Location = new System.Drawing.Point(348, 230);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(293, 187);
            this.groupBox8.TabIndex = 3;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "groupBox8";
            // 
            // B2G4Max
            // 
            this.B2G4Max.Location = new System.Drawing.Point(215, 92);
            this.B2G4Max.Name = "B2G4Max";
            this.B2G4Max.Size = new System.Drawing.Size(43, 20);
            this.B2G4Max.TabIndex = 1;
            // 
            // B2G4Min
            // 
            this.B2G4Min.Location = new System.Drawing.Point(45, 91);
            this.B2G4Min.Name = "B2G4Min";
            this.B2G4Min.Size = new System.Drawing.Size(41, 20);
            this.B2G4Min.TabIndex = 0;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.B2G3Min);
            this.groupBox7.Controls.Add(this.B2GMin);
            this.groupBox7.Location = new System.Drawing.Point(31, 230);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(292, 187);
            this.groupBox7.TabIndex = 2;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "groupBox7";
            // 
            // B2G3Min
            // 
            this.B2G3Min.Location = new System.Drawing.Point(184, 92);
            this.B2G3Min.Name = "B2G3Min";
            this.B2G3Min.Size = new System.Drawing.Size(41, 20);
            this.B2G3Min.TabIndex = 1;
            // 
            // B2GMin
            // 
            this.B2GMin.Location = new System.Drawing.Point(32, 92);
            this.B2GMin.Name = "B2GMin";
            this.B2GMin.Size = new System.Drawing.Size(47, 20);
            this.B2GMin.TabIndex = 0;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.B2G2Max);
            this.groupBox6.Controls.Add(this.B2G2Min);
            this.groupBox6.Location = new System.Drawing.Point(348, 27);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(293, 171);
            this.groupBox6.TabIndex = 1;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "groupBox6";
            // 
            // B2G2Max
            // 
            this.B2G2Max.Location = new System.Drawing.Point(220, 73);
            this.B2G2Max.Name = "B2G2Max";
            this.B2G2Max.Size = new System.Drawing.Size(38, 20);
            this.B2G2Max.TabIndex = 1;
            // 
            // B2G2Min
            // 
            this.B2G2Min.Location = new System.Drawing.Point(34, 73);
            this.B2G2Min.Name = "B2G2Min";
            this.B2G2Min.Size = new System.Drawing.Size(38, 20);
            this.B2G2Min.TabIndex = 0;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.B2G1Max);
            this.groupBox5.Controls.Add(this.B2G1Min);
            this.groupBox5.Location = new System.Drawing.Point(31, 27);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(292, 171);
            this.groupBox5.TabIndex = 0;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "groupBox5";
            // 
            // B2G1Max
            // 
            this.B2G1Max.Location = new System.Drawing.Point(196, 74);
            this.B2G1Max.Name = "B2G1Max";
            this.B2G1Max.Size = new System.Drawing.Size(42, 20);
            this.B2G1Max.TabIndex = 1;
            // 
            // B2G1Min
            // 
            this.B2G1Min.Location = new System.Drawing.Point(41, 74);
            this.B2G1Min.Name = "B2G1Min";
            this.B2G1Min.Size = new System.Drawing.Size(38, 20);
            this.B2G1Min.TabIndex = 0;
            // 
            // tabBay3
            // 
            this.tabBay3.Controls.Add(this.groupBox12);
            this.tabBay3.Controls.Add(this.groupBox11);
            this.tabBay3.Controls.Add(this.groupBox10);
            this.tabBay3.Controls.Add(this.groupBox9);
            this.tabBay3.Location = new System.Drawing.Point(4, 22);
            this.tabBay3.Name = "tabBay3";
            this.tabBay3.Size = new System.Drawing.Size(667, 485);
            this.tabBay3.TabIndex = 2;
            this.tabBay3.Text = "Bay 3";
            this.tabBay3.UseVisualStyleBackColor = true;
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this.B3G4Max);
            this.groupBox12.Controls.Add(this.B3G4Min);
            this.groupBox12.Location = new System.Drawing.Point(345, 249);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(293, 212);
            this.groupBox12.TabIndex = 3;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "groupBox12";
            // 
            // B3G4Max
            // 
            this.B3G4Max.Location = new System.Drawing.Point(208, 105);
            this.B3G4Max.Name = "B3G4Max";
            this.B3G4Max.Size = new System.Drawing.Size(43, 20);
            this.B3G4Max.TabIndex = 1;
            // 
            // B3G4Min
            // 
            this.B3G4Min.Location = new System.Drawing.Point(28, 105);
            this.B3G4Min.Name = "B3G4Min";
            this.B3G4Min.Size = new System.Drawing.Size(41, 20);
            this.B3G4Min.TabIndex = 0;
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.B3G3Max);
            this.groupBox11.Controls.Add(this.B3G3Min);
            this.groupBox11.Location = new System.Drawing.Point(23, 249);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(303, 212);
            this.groupBox11.TabIndex = 2;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "groupBox11";
            // 
            // B3G3Max
            // 
            this.B3G3Max.Location = new System.Drawing.Point(216, 105);
            this.B3G3Max.Name = "B3G3Max";
            this.B3G3Max.Size = new System.Drawing.Size(41, 20);
            this.B3G3Max.TabIndex = 1;
            // 
            // B3G3Min
            // 
            this.B3G3Min.Location = new System.Drawing.Point(36, 105);
            this.B3G3Min.Name = "B3G3Min";
            this.B3G3Min.Size = new System.Drawing.Size(43, 20);
            this.B3G3Min.TabIndex = 0;
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.B3G2Max);
            this.groupBox10.Controls.Add(this.B3G2Min);
            this.groupBox10.Location = new System.Drawing.Point(345, 21);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(293, 202);
            this.groupBox10.TabIndex = 1;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "groupBox10";
            // 
            // B3G2Max
            // 
            this.B3G2Max.Location = new System.Drawing.Point(208, 91);
            this.B3G2Max.Name = "B3G2Max";
            this.B3G2Max.Size = new System.Drawing.Size(45, 20);
            this.B3G2Max.TabIndex = 1;
            // 
            // B3G2Min
            // 
            this.B3G2Min.Location = new System.Drawing.Point(28, 90);
            this.B3G2Min.Name = "B3G2Min";
            this.B3G2Min.Size = new System.Drawing.Size(41, 20);
            this.B3G2Min.TabIndex = 0;
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.B3G1Max);
            this.groupBox9.Controls.Add(this.B3G1Min);
            this.groupBox9.Location = new System.Drawing.Point(23, 21);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(303, 202);
            this.groupBox9.TabIndex = 0;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "groupBox9";
            // 
            // B3G1Max
            // 
            this.B3G1Max.Location = new System.Drawing.Point(189, 90);
            this.B3G1Max.Name = "B3G1Max";
            this.B3G1Max.Size = new System.Drawing.Size(41, 20);
            this.B3G1Max.TabIndex = 1;
            // 
            // B3G1Min
            // 
            this.B3G1Min.Location = new System.Drawing.Point(36, 91);
            this.B3G1Min.Name = "B3G1Min";
            this.B3G1Min.Size = new System.Drawing.Size(37, 20);
            this.B3G1Min.TabIndex = 0;
            // 
            // tabBay4
            // 
            this.tabBay4.Controls.Add(this.groupBox16);
            this.tabBay4.Controls.Add(this.groupBox15);
            this.tabBay4.Controls.Add(this.groupBox14);
            this.tabBay4.Controls.Add(this.groupBox13);
            this.tabBay4.Location = new System.Drawing.Point(4, 22);
            this.tabBay4.Name = "tabBay4";
            this.tabBay4.Size = new System.Drawing.Size(667, 485);
            this.tabBay4.TabIndex = 3;
            this.tabBay4.Text = "Bay4";
            this.tabBay4.UseVisualStyleBackColor = true;
            // 
            // tabBay5
            // 
            this.tabBay5.Controls.Add(this.groupBox20);
            this.tabBay5.Controls.Add(this.groupBox19);
            this.tabBay5.Controls.Add(this.groupBox18);
            this.tabBay5.Controls.Add(this.groupBox17);
            this.tabBay5.Location = new System.Drawing.Point(4, 22);
            this.tabBay5.Name = "tabBay5";
            this.tabBay5.Size = new System.Drawing.Size(667, 485);
            this.tabBay5.TabIndex = 4;
            this.tabBay5.Text = "Bay 5";
            this.tabBay5.UseVisualStyleBackColor = true;
            // 
            // tabBay6
            // 
            this.tabBay6.Controls.Add(this.groupBox24);
            this.tabBay6.Controls.Add(this.groupBox23);
            this.tabBay6.Controls.Add(this.groupBox22);
            this.tabBay6.Controls.Add(this.groupBox21);
            this.tabBay6.Location = new System.Drawing.Point(4, 22);
            this.tabBay6.Name = "tabBay6";
            this.tabBay6.Size = new System.Drawing.Size(667, 485);
            this.tabBay6.TabIndex = 5;
            this.tabBay6.Text = "Bay 6";
            this.tabBay6.UseVisualStyleBackColor = true;
            // 
            // tabBay7
            // 
            this.tabBay7.Controls.Add(this.groupBox28);
            this.tabBay7.Controls.Add(this.groupBox27);
            this.tabBay7.Controls.Add(this.groupBox26);
            this.tabBay7.Controls.Add(this.groupBox25);
            this.tabBay7.Location = new System.Drawing.Point(4, 22);
            this.tabBay7.Name = "tabBay7";
            this.tabBay7.Size = new System.Drawing.Size(667, 485);
            this.tabBay7.TabIndex = 6;
            this.tabBay7.Text = "Bay 7";
            this.tabBay7.UseVisualStyleBackColor = true;
            // 
            // tabBay8
            // 
            this.tabBay8.Controls.Add(this.groupBox32);
            this.tabBay8.Controls.Add(this.groupBox31);
            this.tabBay8.Controls.Add(this.groupBox30);
            this.tabBay8.Controls.Add(this.groupBox29);
            this.tabBay8.Location = new System.Drawing.Point(4, 22);
            this.tabBay8.Name = "tabBay8";
            this.tabBay8.Size = new System.Drawing.Size(667, 485);
            this.tabBay8.TabIndex = 7;
            this.tabBay8.Text = "Bay 8";
            this.tabBay8.UseVisualStyleBackColor = true;
            // 
            // btnClockIn
            // 
            this.btnClockIn.Location = new System.Drawing.Point(696, 203);
            this.btnClockIn.Name = "btnClockIn";
            this.btnClockIn.Size = new System.Drawing.Size(117, 29);
            this.btnClockIn.TabIndex = 1;
            this.btnClockIn.Text = "Clock In";
            this.btnClockIn.UseVisualStyleBackColor = true;
            this.btnClockIn.Click += new System.EventHandler(this.btnClockIn_Click);
            // 
            // btnClockOut
            // 
            this.btnClockOut.Location = new System.Drawing.Point(696, 241);
            this.btnClockOut.Name = "btnClockOut";
            this.btnClockOut.Size = new System.Drawing.Size(117, 29);
            this.btnClockOut.TabIndex = 2;
            this.btnClockOut.Text = "Clock Out";
            this.btnClockOut.UseVisualStyleBackColor = true;
            this.btnClockOut.Click += new System.EventHandler(this.btnClockOut_Click);
            // 
            // lstOnCall
            // 
            this.lstOnCall.FormattingEnabled = true;
            this.lstOnCall.Location = new System.Drawing.Point(696, 34);
            this.lstOnCall.Name = "lstOnCall";
            this.lstOnCall.Size = new System.Drawing.Size(117, 160);
            this.lstOnCall.TabIndex = 3;
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this.B4G1Max);
            this.groupBox13.Controls.Add(this.B4G1Min);
            this.groupBox13.Location = new System.Drawing.Point(27, 21);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(318, 201);
            this.groupBox13.TabIndex = 0;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "groupBox13";
            // 
            // groupBox14
            // 
            this.groupBox14.Controls.Add(this.B4G2Max);
            this.groupBox14.Controls.Add(this.B4G2Min);
            this.groupBox14.Location = new System.Drawing.Point(374, 19);
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.Size = new System.Drawing.Size(272, 202);
            this.groupBox14.TabIndex = 1;
            this.groupBox14.TabStop = false;
            this.groupBox14.Text = "groupBox14";
            // 
            // groupBox15
            // 
            this.groupBox15.Controls.Add(this.B4G3Max);
            this.groupBox15.Controls.Add(this.B4G3Min);
            this.groupBox15.Location = new System.Drawing.Point(27, 252);
            this.groupBox15.Name = "groupBox15";
            this.groupBox15.Size = new System.Drawing.Size(318, 188);
            this.groupBox15.TabIndex = 2;
            this.groupBox15.TabStop = false;
            this.groupBox15.Text = "groupBox15";
            // 
            // groupBox16
            // 
            this.groupBox16.Controls.Add(this.B4G4Max);
            this.groupBox16.Controls.Add(this.B4G4Min);
            this.groupBox16.Location = new System.Drawing.Point(379, 252);
            this.groupBox16.Name = "groupBox16";
            this.groupBox16.Size = new System.Drawing.Size(266, 187);
            this.groupBox16.TabIndex = 3;
            this.groupBox16.TabStop = false;
            this.groupBox16.Text = "groupBox16";
            // 
            // B4G1Min
            // 
            this.B4G1Min.Location = new System.Drawing.Point(32, 96);
            this.B4G1Min.Name = "B4G1Min";
            this.B4G1Min.Size = new System.Drawing.Size(47, 20);
            this.B4G1Min.TabIndex = 0;
            // 
            // B4G1Max
            // 
            this.B4G1Max.Location = new System.Drawing.Point(231, 95);
            this.B4G1Max.Name = "B4G1Max";
            this.B4G1Max.Size = new System.Drawing.Size(42, 20);
            this.B4G1Max.TabIndex = 1;
            // 
            // B4G2Min
            // 
            this.B4G2Min.Location = new System.Drawing.Point(23, 97);
            this.B4G2Min.Name = "B4G2Min";
            this.B4G2Min.Size = new System.Drawing.Size(46, 20);
            this.B4G2Min.TabIndex = 0;
            // 
            // B4G2Max
            // 
            this.B4G2Max.Location = new System.Drawing.Point(202, 98);
            this.B4G2Max.Name = "B4G2Max";
            this.B4G2Max.Size = new System.Drawing.Size(38, 20);
            this.B4G2Max.TabIndex = 1;
            // 
            // B4G3Min
            // 
            this.B4G3Min.Location = new System.Drawing.Point(32, 91);
            this.B4G3Min.Name = "B4G3Min";
            this.B4G3Min.Size = new System.Drawing.Size(47, 20);
            this.B4G3Min.TabIndex = 0;
            // 
            // B4G3Max
            // 
            this.B4G3Max.Location = new System.Drawing.Point(232, 91);
            this.B4G3Max.Name = "B4G3Max";
            this.B4G3Max.Size = new System.Drawing.Size(41, 20);
            this.B4G3Max.TabIndex = 1;
            // 
            // B4G4Min
            // 
            this.B4G4Min.Location = new System.Drawing.Point(29, 91);
            this.B4G4Min.Name = "B4G4Min";
            this.B4G4Min.Size = new System.Drawing.Size(46, 20);
            this.B4G4Min.TabIndex = 0;
            // 
            // B4G4Max
            // 
            this.B4G4Max.Location = new System.Drawing.Point(197, 91);
            this.B4G4Max.Name = "B4G4Max";
            this.B4G4Max.Size = new System.Drawing.Size(40, 20);
            this.B4G4Max.TabIndex = 1;
            // 
            // groupBox17
            // 
            this.groupBox17.Controls.Add(this.B5G1Max);
            this.groupBox17.Controls.Add(this.B5G1Min);
            this.groupBox17.Location = new System.Drawing.Point(25, 24);
            this.groupBox17.Name = "groupBox17";
            this.groupBox17.Size = new System.Drawing.Size(304, 212);
            this.groupBox17.TabIndex = 0;
            this.groupBox17.TabStop = false;
            this.groupBox17.Text = "groupBox17";
            // 
            // groupBox18
            // 
            this.groupBox18.Controls.Add(this.B5G2Max);
            this.groupBox18.Controls.Add(this.B5G2Min);
            this.groupBox18.Location = new System.Drawing.Point(356, 24);
            this.groupBox18.Name = "groupBox18";
            this.groupBox18.Size = new System.Drawing.Size(284, 212);
            this.groupBox18.TabIndex = 1;
            this.groupBox18.TabStop = false;
            this.groupBox18.Text = "groupBox18";
            // 
            // groupBox19
            // 
            this.groupBox19.Controls.Add(this.B5G3Max);
            this.groupBox19.Controls.Add(this.B5G3Min);
            this.groupBox19.Location = new System.Drawing.Point(25, 260);
            this.groupBox19.Name = "groupBox19";
            this.groupBox19.Size = new System.Drawing.Size(304, 203);
            this.groupBox19.TabIndex = 2;
            this.groupBox19.TabStop = false;
            this.groupBox19.Text = "groupBox19";
            // 
            // groupBox20
            // 
            this.groupBox20.Controls.Add(this.B5G4Max);
            this.groupBox20.Controls.Add(this.B5G4Min);
            this.groupBox20.Location = new System.Drawing.Point(356, 260);
            this.groupBox20.Name = "groupBox20";
            this.groupBox20.Size = new System.Drawing.Size(284, 203);
            this.groupBox20.TabIndex = 3;
            this.groupBox20.TabStop = false;
            this.groupBox20.Text = "groupBox20";
            // 
            // B5G1Min
            // 
            this.B5G1Min.Location = new System.Drawing.Point(34, 96);
            this.B5G1Min.Name = "B5G1Min";
            this.B5G1Min.Size = new System.Drawing.Size(41, 20);
            this.B5G1Min.TabIndex = 0;
            // 
            // B5G1Max
            // 
            this.B5G1Max.Location = new System.Drawing.Point(222, 96);
            this.B5G1Max.Name = "B5G1Max";
            this.B5G1Max.Size = new System.Drawing.Size(41, 20);
            this.B5G1Max.TabIndex = 1;
            // 
            // B5G2Min
            // 
            this.B5G2Min.Location = new System.Drawing.Point(32, 96);
            this.B5G2Min.Name = "B5G2Min";
            this.B5G2Min.Size = new System.Drawing.Size(40, 20);
            this.B5G2Min.TabIndex = 0;
            // 
            // B5G2Max
            // 
            this.B5G2Max.Location = new System.Drawing.Point(203, 96);
            this.B5G2Max.Name = "B5G2Max";
            this.B5G2Max.Size = new System.Drawing.Size(38, 20);
            this.B5G2Max.TabIndex = 1;
            // 
            // B5G3Max
            // 
            this.B5G3Max.Location = new System.Drawing.Point(222, 91);
            this.B5G3Max.Name = "B5G3Max";
            this.B5G3Max.Size = new System.Drawing.Size(41, 20);
            this.B5G3Max.TabIndex = 3;
            // 
            // B5G3Min
            // 
            this.B5G3Min.Location = new System.Drawing.Point(34, 91);
            this.B5G3Min.Name = "B5G3Min";
            this.B5G3Min.Size = new System.Drawing.Size(41, 20);
            this.B5G3Min.TabIndex = 2;
            // 
            // B5G4Max
            // 
            this.B5G4Max.Location = new System.Drawing.Point(203, 91);
            this.B5G4Max.Name = "B5G4Max";
            this.B5G4Max.Size = new System.Drawing.Size(41, 20);
            this.B5G4Max.TabIndex = 5;
            // 
            // B5G4Min
            // 
            this.B5G4Min.Location = new System.Drawing.Point(15, 91);
            this.B5G4Min.Name = "B5G4Min";
            this.B5G4Min.Size = new System.Drawing.Size(41, 20);
            this.B5G4Min.TabIndex = 4;
            // 
            // groupBox21
            // 
            this.groupBox21.Controls.Add(this.B6G1Max);
            this.groupBox21.Controls.Add(this.B6G1Min);
            this.groupBox21.Location = new System.Drawing.Point(23, 25);
            this.groupBox21.Name = "groupBox21";
            this.groupBox21.Size = new System.Drawing.Size(306, 194);
            this.groupBox21.TabIndex = 0;
            this.groupBox21.TabStop = false;
            this.groupBox21.Text = "groupBox21";
            // 
            // groupBox22
            // 
            this.groupBox22.Controls.Add(this.B6G2Max);
            this.groupBox22.Controls.Add(this.B6G2Min);
            this.groupBox22.Location = new System.Drawing.Point(346, 25);
            this.groupBox22.Name = "groupBox22";
            this.groupBox22.Size = new System.Drawing.Size(293, 194);
            this.groupBox22.TabIndex = 1;
            this.groupBox22.TabStop = false;
            this.groupBox22.Text = "groupBox22";
            // 
            // groupBox23
            // 
            this.groupBox23.Controls.Add(this.B6G3Max);
            this.groupBox23.Controls.Add(this.B6G3Min);
            this.groupBox23.Location = new System.Drawing.Point(23, 248);
            this.groupBox23.Name = "groupBox23";
            this.groupBox23.Size = new System.Drawing.Size(300, 208);
            this.groupBox23.TabIndex = 2;
            this.groupBox23.TabStop = false;
            this.groupBox23.Text = "groupBox23";
            // 
            // groupBox24
            // 
            this.groupBox24.Controls.Add(this.B6G4Max);
            this.groupBox24.Controls.Add(this.B6G4Min);
            this.groupBox24.Location = new System.Drawing.Point(346, 248);
            this.groupBox24.Name = "groupBox24";
            this.groupBox24.Size = new System.Drawing.Size(293, 208);
            this.groupBox24.TabIndex = 3;
            this.groupBox24.TabStop = false;
            this.groupBox24.Text = "groupBox24";
            // 
            // B6G1Min
            // 
            this.B6G1Min.Location = new System.Drawing.Point(41, 87);
            this.B6G1Min.Name = "B6G1Min";
            this.B6G1Min.Size = new System.Drawing.Size(42, 20);
            this.B6G1Min.TabIndex = 0;
            // 
            // B6G1Max
            // 
            this.B6G1Max.Location = new System.Drawing.Point(218, 87);
            this.B6G1Max.Name = "B6G1Max";
            this.B6G1Max.Size = new System.Drawing.Size(44, 20);
            this.B6G1Max.TabIndex = 1;
            // 
            // B6G2Max
            // 
            this.B6G2Max.Location = new System.Drawing.Point(215, 87);
            this.B6G2Max.Name = "B6G2Max";
            this.B6G2Max.Size = new System.Drawing.Size(44, 20);
            this.B6G2Max.TabIndex = 3;
            // 
            // B6G2Min
            // 
            this.B6G2Min.Location = new System.Drawing.Point(38, 87);
            this.B6G2Min.Name = "B6G2Min";
            this.B6G2Min.Size = new System.Drawing.Size(42, 20);
            this.B6G2Min.TabIndex = 2;
            // 
            // B6G4Max
            // 
            this.B6G4Max.Location = new System.Drawing.Point(215, 102);
            this.B6G4Max.Name = "B6G4Max";
            this.B6G4Max.Size = new System.Drawing.Size(44, 20);
            this.B6G4Max.TabIndex = 5;
            // 
            // B6G4Min
            // 
            this.B6G4Min.Location = new System.Drawing.Point(38, 102);
            this.B6G4Min.Name = "B6G4Min";
            this.B6G4Min.Size = new System.Drawing.Size(42, 20);
            this.B6G4Min.TabIndex = 4;
            // 
            // B6G3Max
            // 
            this.B6G3Max.Location = new System.Drawing.Point(218, 102);
            this.B6G3Max.Name = "B6G3Max";
            this.B6G3Max.Size = new System.Drawing.Size(44, 20);
            this.B6G3Max.TabIndex = 7;
            // 
            // B6G3Min
            // 
            this.B6G3Min.Location = new System.Drawing.Point(41, 102);
            this.B6G3Min.Name = "B6G3Min";
            this.B6G3Min.Size = new System.Drawing.Size(42, 20);
            this.B6G3Min.TabIndex = 6;
            // 
            // groupBox25
            // 
            this.groupBox25.Controls.Add(this.B7G1Max);
            this.groupBox25.Controls.Add(this.B7G1Min);
            this.groupBox25.Location = new System.Drawing.Point(24, 30);
            this.groupBox25.Name = "groupBox25";
            this.groupBox25.Size = new System.Drawing.Size(299, 183);
            this.groupBox25.TabIndex = 0;
            this.groupBox25.TabStop = false;
            this.groupBox25.Text = "groupBox25";
            // 
            // groupBox26
            // 
            this.groupBox26.Controls.Add(this.B7G2Max);
            this.groupBox26.Controls.Add(this.B7G2Min);
            this.groupBox26.Location = new System.Drawing.Point(356, 30);
            this.groupBox26.Name = "groupBox26";
            this.groupBox26.Size = new System.Drawing.Size(283, 182);
            this.groupBox26.TabIndex = 1;
            this.groupBox26.TabStop = false;
            this.groupBox26.Text = "groupBox26";
            // 
            // groupBox27
            // 
            this.groupBox27.Controls.Add(this.B7G3Max);
            this.groupBox27.Controls.Add(this.B7G3Min);
            this.groupBox27.Location = new System.Drawing.Point(24, 250);
            this.groupBox27.Name = "groupBox27";
            this.groupBox27.Size = new System.Drawing.Size(295, 209);
            this.groupBox27.TabIndex = 2;
            this.groupBox27.TabStop = false;
            this.groupBox27.Text = "groupBox27";
            // 
            // groupBox28
            // 
            this.groupBox28.Controls.Add(this.B7G4Max);
            this.groupBox28.Controls.Add(this.B7G4Min);
            this.groupBox28.Location = new System.Drawing.Point(365, 250);
            this.groupBox28.Name = "groupBox28";
            this.groupBox28.Size = new System.Drawing.Size(273, 208);
            this.groupBox28.TabIndex = 3;
            this.groupBox28.TabStop = false;
            this.groupBox28.Text = "groupBox28";
            // 
            // B7G1Min
            // 
            this.B7G1Min.Location = new System.Drawing.Point(33, 85);
            this.B7G1Min.Name = "B7G1Min";
            this.B7G1Min.Size = new System.Drawing.Size(41, 20);
            this.B7G1Min.TabIndex = 0;
            // 
            // B7G1Max
            // 
            this.B7G1Max.Location = new System.Drawing.Point(213, 85);
            this.B7G1Max.Name = "B7G1Max";
            this.B7G1Max.Size = new System.Drawing.Size(42, 20);
            this.B7G1Max.TabIndex = 1;
            // 
            // B7G3Max
            // 
            this.B7G3Max.Location = new System.Drawing.Point(213, 112);
            this.B7G3Max.Name = "B7G3Max";
            this.B7G3Max.Size = new System.Drawing.Size(42, 20);
            this.B7G3Max.TabIndex = 3;
            // 
            // B7G3Min
            // 
            this.B7G3Min.Location = new System.Drawing.Point(33, 112);
            this.B7G3Min.Name = "B7G3Min";
            this.B7G3Min.Size = new System.Drawing.Size(41, 20);
            this.B7G3Min.TabIndex = 2;
            // 
            // B7G4Max
            // 
            this.B7G4Max.Location = new System.Drawing.Point(209, 112);
            this.B7G4Max.Name = "B7G4Max";
            this.B7G4Max.Size = new System.Drawing.Size(42, 20);
            this.B7G4Max.TabIndex = 5;
            // 
            // B7G4Min
            // 
            this.B7G4Min.Location = new System.Drawing.Point(29, 112);
            this.B7G4Min.Name = "B7G4Min";
            this.B7G4Min.Size = new System.Drawing.Size(41, 20);
            this.B7G4Min.TabIndex = 4;
            // 
            // B7G2Max
            // 
            this.B7G2Max.Location = new System.Drawing.Point(201, 85);
            this.B7G2Max.Name = "B7G2Max";
            this.B7G2Max.Size = new System.Drawing.Size(42, 20);
            this.B7G2Max.TabIndex = 7;
            // 
            // B7G2Min
            // 
            this.B7G2Min.Location = new System.Drawing.Point(21, 85);
            this.B7G2Min.Name = "B7G2Min";
            this.B7G2Min.Size = new System.Drawing.Size(41, 20);
            this.B7G2Min.TabIndex = 6;
            // 
            // groupBox29
            // 
            this.groupBox29.Controls.Add(this.B8G1Max);
            this.groupBox29.Controls.Add(this.B8G1Min);
            this.groupBox29.Location = new System.Drawing.Point(22, 22);
            this.groupBox29.Name = "groupBox29";
            this.groupBox29.Size = new System.Drawing.Size(310, 214);
            this.groupBox29.TabIndex = 0;
            this.groupBox29.TabStop = false;
            this.groupBox29.Text = "groupBox29";
            // 
            // groupBox30
            // 
            this.groupBox30.Controls.Add(this.B8G2Max);
            this.groupBox30.Controls.Add(this.B8G2Min);
            this.groupBox30.Location = new System.Drawing.Point(353, 22);
            this.groupBox30.Name = "groupBox30";
            this.groupBox30.Size = new System.Drawing.Size(287, 214);
            this.groupBox30.TabIndex = 1;
            this.groupBox30.TabStop = false;
            this.groupBox30.Text = "groupBox30";
            // 
            // groupBox31
            // 
            this.groupBox31.Controls.Add(this.B8G3Max);
            this.groupBox31.Controls.Add(this.B8G3Min);
            this.groupBox31.Location = new System.Drawing.Point(22, 262);
            this.groupBox31.Name = "groupBox31";
            this.groupBox31.Size = new System.Drawing.Size(310, 198);
            this.groupBox31.TabIndex = 2;
            this.groupBox31.TabStop = false;
            this.groupBox31.Text = "groupBox31";
            // 
            // groupBox32
            // 
            this.groupBox32.Controls.Add(this.B8G4Max);
            this.groupBox32.Controls.Add(this.B8G4Min);
            this.groupBox32.Location = new System.Drawing.Point(353, 262);
            this.groupBox32.Name = "groupBox32";
            this.groupBox32.Size = new System.Drawing.Size(287, 198);
            this.groupBox32.TabIndex = 3;
            this.groupBox32.TabStop = false;
            this.groupBox32.Text = "groupBox32";
            // 
            // B8G1Min
            // 
            this.B8G1Min.Location = new System.Drawing.Point(37, 103);
            this.B8G1Min.Name = "B8G1Min";
            this.B8G1Min.Size = new System.Drawing.Size(43, 20);
            this.B8G1Min.TabIndex = 0;
            // 
            // B8G1Max
            // 
            this.B8G1Max.Location = new System.Drawing.Point(214, 103);
            this.B8G1Max.Name = "B8G1Max";
            this.B8G1Max.Size = new System.Drawing.Size(48, 20);
            this.B8G1Max.TabIndex = 1;
            // 
            // B8G2Max
            // 
            this.B8G2Max.Location = new System.Drawing.Point(208, 103);
            this.B8G2Max.Name = "B8G2Max";
            this.B8G2Max.Size = new System.Drawing.Size(48, 20);
            this.B8G2Max.TabIndex = 3;
            // 
            // B8G2Min
            // 
            this.B8G2Min.Location = new System.Drawing.Point(31, 103);
            this.B8G2Min.Name = "B8G2Min";
            this.B8G2Min.Size = new System.Drawing.Size(43, 20);
            this.B8G2Min.TabIndex = 2;
            // 
            // B8G3Max
            // 
            this.B8G3Max.Location = new System.Drawing.Point(214, 100);
            this.B8G3Max.Name = "B8G3Max";
            this.B8G3Max.Size = new System.Drawing.Size(48, 20);
            this.B8G3Max.TabIndex = 5;
            // 
            // B8G3Min
            // 
            this.B8G3Min.Location = new System.Drawing.Point(37, 100);
            this.B8G3Min.Name = "B8G3Min";
            this.B8G3Min.Size = new System.Drawing.Size(43, 20);
            this.B8G3Min.TabIndex = 4;
            // 
            // B8G4Max
            // 
            this.B8G4Max.Location = new System.Drawing.Point(208, 100);
            this.B8G4Max.Name = "B8G4Max";
            this.B8G4Max.Size = new System.Drawing.Size(48, 20);
            this.B8G4Max.TabIndex = 7;
            // 
            // B8G4Min
            // 
            this.B8G4Min.Location = new System.Drawing.Point(31, 100);
            this.B8G4Min.Name = "B8G4Min";
            this.B8G4Min.Size = new System.Drawing.Size(43, 20);
            this.B8G4Min.TabIndex = 6;
            // 
            // PatientMonitoringMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(831, 535);
            this.Controls.Add(this.lstOnCall);
            this.Controls.Add(this.btnClockOut);
            this.Controls.Add(this.btnClockIn);
            this.Controls.Add(this.tabControl1);
            this.Name = "PatientMonitoringMain";
            this.Text = "Patient Monitoring Station";
            this.Load += new System.EventHandler(this.PatientMonitoringMain_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabBay1.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.B1G4Max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.B1G4Min)).EndInit();
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.B1G3Max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.B1G3Min)).EndInit();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.B1G2Max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.B1G2Min)).EndInit();
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.B1G1Min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.B1G1Max)).EndInit();
            this.tabBay2.ResumeLayout(false);
            this.groupBox8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.B2G4Max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.B2G4Min)).EndInit();
            this.groupBox7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.B2G3Min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.B2GMin)).EndInit();
            this.groupBox6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.B2G2Max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.B2G2Min)).EndInit();
            this.groupBox5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.B2G1Max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.B2G1Min)).EndInit();
            this.tabBay3.ResumeLayout(false);
            this.groupBox12.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.B3G4Max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.B3G4Min)).EndInit();
            this.groupBox11.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.B3G3Max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.B3G3Min)).EndInit();
            this.groupBox10.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.B3G2Max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.B3G2Min)).EndInit();
            this.groupBox9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.B3G1Max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.B3G1Min)).EndInit();
            this.tabBay4.ResumeLayout(false);
            this.tabBay5.ResumeLayout(false);
            this.tabBay6.ResumeLayout(false);
            this.tabBay7.ResumeLayout(false);
            this.tabBay8.ResumeLayout(false);
            this.groupBox13.ResumeLayout(false);
            this.groupBox14.ResumeLayout(false);
            this.groupBox15.ResumeLayout(false);
            this.groupBox16.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.B4G1Min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.B4G1Max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.B4G2Min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.B4G2Max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.B4G3Min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.B4G3Max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.B4G4Min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.B4G4Max)).EndInit();
            this.groupBox17.ResumeLayout(false);
            this.groupBox18.ResumeLayout(false);
            this.groupBox19.ResumeLayout(false);
            this.groupBox20.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.B5G1Min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.B5G1Max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.B5G2Min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.B5G2Max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.B5G3Max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.B5G3Min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.B5G4Max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.B5G4Min)).EndInit();
            this.groupBox21.ResumeLayout(false);
            this.groupBox22.ResumeLayout(false);
            this.groupBox23.ResumeLayout(false);
            this.groupBox24.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.B6G1Min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.B6G1Max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.B6G2Max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.B6G2Min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.B6G4Max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.B6G4Min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.B6G3Max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.B6G3Min)).EndInit();
            this.groupBox25.ResumeLayout(false);
            this.groupBox26.ResumeLayout(false);
            this.groupBox27.ResumeLayout(false);
            this.groupBox28.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.B7G1Min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.B7G1Max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.B7G3Max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.B7G3Min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.B7G4Max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.B7G4Min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.B7G2Max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.B7G2Min)).EndInit();
            this.groupBox29.ResumeLayout(false);
            this.groupBox30.ResumeLayout(false);
            this.groupBox31.ResumeLayout(false);
            this.groupBox32.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.B8G1Min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.B8G1Max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.B8G2Max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.B8G2Min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.B8G3Max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.B8G3Min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.B8G4Max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.B8G4Min)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabBay1;
        private System.Windows.Forms.TabPage tabBay2;
        private System.Windows.Forms.TabPage tabBay3;
        private System.Windows.Forms.TabPage tabBay4;
        private System.Windows.Forms.TabPage tabBay5;
        private System.Windows.Forms.TabPage tabBay6;
        private System.Windows.Forms.TabPage tabBay7;
        private System.Windows.Forms.TabPage tabBay8;
        private System.Windows.Forms.Button btnClockIn;
        private System.Windows.Forms.Button btnClockOut;
        private System.Windows.Forms.ListBox lstOnCall;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.NumericUpDown B1G1Max;
        private System.Windows.Forms.NumericUpDown B1G1Min;
        private System.Windows.Forms.NumericUpDown B1G4Max;
        private System.Windows.Forms.NumericUpDown B1G4Min;
        private System.Windows.Forms.NumericUpDown B1G3Max;
        private System.Windows.Forms.NumericUpDown B1G3Min;
        private System.Windows.Forms.NumericUpDown B1G2Max;
        private System.Windows.Forms.NumericUpDown B1G2Min;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.NumericUpDown B2G4Max;
        private System.Windows.Forms.NumericUpDown B2G4Min;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.NumericUpDown B2G3Min;
        private System.Windows.Forms.NumericUpDown B2GMin;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.NumericUpDown B2G2Max;
        private System.Windows.Forms.NumericUpDown B2G2Min;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.NumericUpDown B2G1Max;
        private System.Windows.Forms.NumericUpDown B2G1Min;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.NumericUpDown B3G4Max;
        private System.Windows.Forms.NumericUpDown B3G4Min;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.NumericUpDown B3G3Max;
        private System.Windows.Forms.NumericUpDown B3G3Min;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.NumericUpDown B3G2Max;
        private System.Windows.Forms.NumericUpDown B3G2Min;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.NumericUpDown B3G1Max;
        private System.Windows.Forms.NumericUpDown B3G1Min;
        private System.Windows.Forms.GroupBox groupBox16;
        private System.Windows.Forms.NumericUpDown B4G4Max;
        private System.Windows.Forms.NumericUpDown B4G4Min;
        private System.Windows.Forms.GroupBox groupBox15;
        private System.Windows.Forms.NumericUpDown B4G3Max;
        private System.Windows.Forms.NumericUpDown B4G3Min;
        private System.Windows.Forms.GroupBox groupBox14;
        private System.Windows.Forms.NumericUpDown B4G2Max;
        private System.Windows.Forms.NumericUpDown B4G2Min;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.NumericUpDown B4G1Max;
        private System.Windows.Forms.NumericUpDown B4G1Min;
        private System.Windows.Forms.GroupBox groupBox20;
        private System.Windows.Forms.NumericUpDown B5G4Max;
        private System.Windows.Forms.NumericUpDown B5G4Min;
        private System.Windows.Forms.GroupBox groupBox19;
        private System.Windows.Forms.NumericUpDown B5G3Max;
        private System.Windows.Forms.NumericUpDown B5G3Min;
        private System.Windows.Forms.GroupBox groupBox18;
        private System.Windows.Forms.NumericUpDown B5G2Max;
        private System.Windows.Forms.NumericUpDown B5G2Min;
        private System.Windows.Forms.GroupBox groupBox17;
        private System.Windows.Forms.NumericUpDown B5G1Max;
        private System.Windows.Forms.NumericUpDown B5G1Min;
        private System.Windows.Forms.GroupBox groupBox24;
        private System.Windows.Forms.NumericUpDown B6G4Max;
        private System.Windows.Forms.NumericUpDown B6G4Min;
        private System.Windows.Forms.GroupBox groupBox23;
        private System.Windows.Forms.NumericUpDown B6G3Max;
        private System.Windows.Forms.NumericUpDown B6G3Min;
        private System.Windows.Forms.GroupBox groupBox22;
        private System.Windows.Forms.NumericUpDown B6G2Max;
        private System.Windows.Forms.NumericUpDown B6G2Min;
        private System.Windows.Forms.GroupBox groupBox21;
        private System.Windows.Forms.NumericUpDown B6G1Max;
        private System.Windows.Forms.NumericUpDown B6G1Min;
        private System.Windows.Forms.GroupBox groupBox28;
        private System.Windows.Forms.NumericUpDown B7G4Max;
        private System.Windows.Forms.NumericUpDown B7G4Min;
        private System.Windows.Forms.GroupBox groupBox27;
        private System.Windows.Forms.NumericUpDown B7G3Max;
        private System.Windows.Forms.NumericUpDown B7G3Min;
        private System.Windows.Forms.GroupBox groupBox26;
        private System.Windows.Forms.NumericUpDown B7G2Max;
        private System.Windows.Forms.NumericUpDown B7G2Min;
        private System.Windows.Forms.GroupBox groupBox25;
        private System.Windows.Forms.NumericUpDown B7G1Max;
        private System.Windows.Forms.NumericUpDown B7G1Min;
        private System.Windows.Forms.GroupBox groupBox32;
        private System.Windows.Forms.NumericUpDown B8G4Max;
        private System.Windows.Forms.NumericUpDown B8G4Min;
        private System.Windows.Forms.GroupBox groupBox31;
        private System.Windows.Forms.NumericUpDown B8G3Max;
        private System.Windows.Forms.NumericUpDown B8G3Min;
        private System.Windows.Forms.GroupBox groupBox30;
        private System.Windows.Forms.NumericUpDown B8G2Max;
        private System.Windows.Forms.NumericUpDown B8G2Min;
        private System.Windows.Forms.GroupBox groupBox29;
        private System.Windows.Forms.NumericUpDown B8G1Max;
        private System.Windows.Forms.NumericUpDown B8G1Min;

        //Lines 1377-1474 Implemented by SID: 1410107
        //Naming Convention is Bed Number, Group Number, Type, ie 'B1G1Min' is Bed 1, Group box 1, Min Value.
    }
}

